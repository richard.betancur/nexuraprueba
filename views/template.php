<!doctype html>
<html lang="es">
  <head>
    <title>Prueba Tecnica Nexura</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS v5.0.2 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"  integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  </head>
  <body>
      

<nav class="navbar navbar-expand navbar-light bg-light">
    <div class="nav navbar-nav">
        <a class="nav-item nav-link active" href="#">Nexura <span class="visually-hidden">(current)</span></a>
        <a class="nav-item nav-link" href="?controller=page&action=home">Home</a>
        <a class="nav-item nav-link" href="?controller=employees&action=show">Empleados</a>
    </div>
</nav>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php include_once("root.php") ?>
            </div>
            
        </div>
    </div>



    <!-- Bootstrap JavaScript Libraries -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


    <script>
  $(document).ready(function() {
   $('#frmCreate').validate({
      rules : {
        FullName:{
          required: true,
          minlenght: 10 
        }
      },
      rules : {
        Email:{
          required: true
        }
      },
      rules : {
        Gender:{
          required: true
        }
      },
      rules : {
        Area:{
          required: true
        }
      },
      rules : {
        Description:{
          required: true,
          minlenght: 250 
        }
      },
      messages : {
        FullName: {
          required: "Debe Ingresar un Nombre",
          minlength: "El Nombre Completo debe Tener Minino 10 Caracteres"
      },
      Email: {
        required: "Ingrese Un Email",
        email: "The email should be in the format: abc@domain.tld"
      },
      Gender: {
        required: "Debe Seleccionar una opción"
      },
      Area: {
        required: "Debe Seleccionar una opcion"
      },
      Description: {
        required: "Debe agregar una Descripcion"
      }
    }
   });
});
</script>
  </body>
</html>